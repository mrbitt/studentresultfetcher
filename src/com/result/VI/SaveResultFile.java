/*
 *This code fetches student result page and saves it to temp file,which 
 *will be used for fetching student result info
 * */
package com.result.VI;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class SaveResultFile {
	public void saveToDisk(String roll,String key){
		try{
			URL url = new URL("http://www.rtuportal.com/result/show_result?result_id=331&roll_number="+roll+"&key="+key);
			URLConnection conn = url.openConnection();
			HttpURLConnection hconn = (HttpURLConnection) conn;
			hconn.setRequestMethod("GET");	
			hconn.setRequestProperty("Connection", "keep-alive");
			hconn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			hconn.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			hconn.setRequestProperty("Cache-Control", "max-age=0");
			hconn.setRequestProperty("Cookie", "__cfduid=d920b14fc9a628120ada6b7b16a1e9ec91472788505; _gat=1; XSRF-TOKEN=eyJpdiI6Ikc3MkNNdkw3TEFwMTFPT2t5UEFLVWc9PSIsInZhbHVlIjoieGQxNEFYeDhTOXpoSWYyUm5aRHZ3KzZvMFozV0Vnb3BPR1p6aTgzemFyMzdqYklLMElJTFZoajNjaXpKTHJ2ZExWUUZHNGRmdStZM2ZWMjg4N0FrcVE9PSIsIm1hYyI6ImZiZmU5ZGQwNjI4ZGJmMTE5MzRjNjFjNjYyNjlkMTM1ZmI1MGZhNWVhNWQyMTA2YTNhMTU5ZTVjMmMzOTJjMjYifQ%3D%3D; laravel_session=eyJpdiI6IktMNE4xMWhQODNabEtYdGpcL05Ga3FBPT0iLCJ2YWx1ZSI6IjNiSFI0cE13OWJwNktDWTJUTFExTGZoamZweFg5aG80OEwxM21SNUJKMmxra09ZV29DckRVdzJ6czlSaHV0ajFwbTB5VzJ2RW1mS1QwUVlORjNaWXlRPT0iLCJtYWMiOiJjMzAyZDQxOTdlY2VlNDNlOWVjNDBiMDYyYmM5MmJjZTQ0ZmE3NmZhMWU1NDI5NDk4NWRhZWFlYWQ2N2FlZDJlIn0%3D; _ga=GA1.2.5202674.1472788544");			
			hconn.setRequestProperty("Host", "www.rtuportal.com");
			hconn.setRequestProperty("Upgrade-Insecure-Requests", "1");
			hconn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");			
			hconn.connect();
			BufferedReader bis = new BufferedReader(new InputStreamReader(hconn.getInputStream()));
			File file = new File("temp.txt");
			file.delete();
			BufferedWriter bw = new BufferedWriter(new FileWriter("temp.txt"));
			String str="";
				while((str=bis.readLine())!=null){
					bw.write(str);
					bw.newLine();
				}
				bis.close();
				bw.close();
				hconn.disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
