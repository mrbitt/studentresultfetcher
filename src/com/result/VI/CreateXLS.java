/*
 * This code creates xls file from student results
 */
package com.result.VI;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class CreateXLS {
	public void create(){
			try{
					Pattern p = Pattern.compile(".*(\\-|\\:)(.*)");
					Matcher m;
					BufferedReader r2;
					HSSFWorkbook workbook = new HSSFWorkbook();
					HSSFSheet sheet = workbook.createSheet();
					HSSFRow row;
					int rowCount=0;
					for(int i=1;i<=196;i++){
						System.out.println("reading file "+i);
						
						r2 = new BufferedReader(new FileReader("results/student"+i+".txt"));
						String s="";
						int col=0;
						row = sheet.createRow(rowCount++);
						while((s=r2.readLine())!=null){
							if(col>0){
								m = p.matcher(s);
								if(m.find())
									s=m.group(2);
							}
							row.createCell(col++).setCellValue(s);
						}
						r2.close();
					}
					
					//for autosizing
					for(int i=0;i<15;i++){
						sheet.autoSizeColumn(i);
					}
					
					//generating xls file
			        FileOutputStream fileOut = new FileOutputStream("result.xls");
			        workbook.write(fileOut);
			        
			        fileOut.close();
			        workbook.close();
		}catch(Exception e){
				e.printStackTrace();
		}
	}
}
