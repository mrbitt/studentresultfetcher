/*
 *This code send post request to rtu portal and fetches students unique id
 * */

package com.result.VI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SendPostRequest {
	public String send(String roll){
		String key="";
		Pattern p = Pattern.compile("http\\://www\\.rtuportal\\.com/result/show_result\\?result_id\\=331\\&amp\\;roll_number\\="+roll+"\\&amp\\;key\\=([\\w]+)");
		Matcher m ;
		HashMap<String,String> map = new HashMap<String, String>();
		map.put("roll_number",roll);
		map.put("_token","vbITmENz011kccIevDBD0WJRqgEyTysJD1UBCdtc");
		
		try{
			StringJoiner sj = new StringJoiner("&");
			for(String s:map.keySet()){
				sj.add(URLEncoder.encode(s,"UTF-8")+"="+URLEncoder.encode(map.get(s),"UTF-8"));
			}
			byte[] toSend = sj.toString().getBytes(StandardCharsets.UTF_8);
		
			URL url = new URL("http://www.rtuportal.com/result/331/b-tech-vi-sem-main-exam-2016");
			URLConnection conn = url.openConnection();
			HttpURLConnection hconn = (HttpURLConnection) conn;
			hconn.setRequestMethod("POST");	
			hconn.setRequestProperty("Connection", "keep-alive");
			hconn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			hconn.setRequestProperty("Content-Length", "70");
			hconn.setRequestProperty("Cache-Control", "max-age=0");
			hconn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		//	hconn.setRequestProperty("Accept-Encoding", "gzip, deflate");
			hconn.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			hconn.setRequestProperty("Cookie", "__cfduid=d920b14fc9a628120ada6b7b16a1e9ec91472788505; _gat=1; XSRF-TOKEN=eyJpdiI6Ikc3MkNNdkw3TEFwMTFPT2t5UEFLVWc9PSIsInZhbHVlIjoieGQxNEFYeDhTOXpoSWYyUm5aRHZ3KzZvMFozV0Vnb3BPR1p6aTgzemFyMzdqYklLMElJTFZoajNjaXpKTHJ2ZExWUUZHNGRmdStZM2ZWMjg4N0FrcVE9PSIsIm1hYyI6ImZiZmU5ZGQwNjI4ZGJmMTE5MzRjNjFjNjYyNjlkMTM1ZmI1MGZhNWVhNWQyMTA2YTNhMTU5ZTVjMmMzOTJjMjYifQ%3D%3D; laravel_session=eyJpdiI6IktMNE4xMWhQODNabEtYdGpcL05Ga3FBPT0iLCJ2YWx1ZSI6IjNiSFI0cE13OWJwNktDWTJUTFExTGZoamZweFg5aG80OEwxM21SNUJKMmxra09ZV29DckRVdzJ6czlSaHV0ajFwbTB5VzJ2RW1mS1QwUVlORjNaWXlRPT0iLCJtYWMiOiJjMzAyZDQxOTdlY2VlNDNlOWVjNDBiMDYyYmM5MmJjZTQ0ZmE3NmZhMWU1NDI5NDk4NWRhZWFlYWQ2N2FlZDJlIn0%3D; _ga=GA1.2.5202674.1472788544");			
			hconn.setRequestProperty("Host", "www.rtuportal.com");
			hconn.setRequestProperty("Origin", "http://www.rtuportal.com");
			hconn.setRequestProperty("Referer", "http://www.rtuportal.com/result/331/b-tech-vi-sem-main-exam-2016");
			hconn.setRequestProperty("Upgrade-Insecure-Requests", "1");
			hconn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");			
			hconn.setDoOutput(true);
			hconn.connect();
			OutputStream os = hconn.getOutputStream();
			os.write(toSend);
			os.close();
			BufferedReader bis = new BufferedReader(new InputStreamReader(hconn.getInputStream()));
			String str="";
				while((str=bis.readLine())!=null){
					m = p.matcher(str);
					if(m.find()){
						key=m.group(1);
						break;
					}
				}
				bis.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return key;
	}

}
