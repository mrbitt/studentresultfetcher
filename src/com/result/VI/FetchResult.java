/*
 * This code extracts student result and saves it to files in results directory
 * */
package com.result.VI;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FetchResult {
	public void fetch(int i){
		try{
			String actual="";
			Pattern p = Pattern.compile("NAME :([^╞]+)FATHER'S NAME :[^6]+([^╞]+)\\</table\\>");
			BufferedReader reader = new BufferedReader(new FileReader("temp.txt"));
			BufferedWriter writer = new BufferedWriter(new FileWriter("results/student"+i+".txt"));
			String code = "";
			String s = "";
				while((s=reader.readLine())!=null){
					code+=s+"\n";
				}
				reader.close();
			Matcher m = p.matcher(code);
			if(m.find()){
				code=m.group(1);
				code = code.replaceAll("\\<[^\\<]+\\>"," ");
				Scanner sc = new Scanner(code);
					while(sc.hasNext()){
						actual+=sc.next()+" ";
					}
					actual+="\n";
				sc.close();
				code=m.group(2);
				code = code.replaceAll("\\<[^\\<]+\\>"," ");
				sc = new Scanner(code);
					while(sc.hasNext()){
						actual+=sc.next()+"\n";
					}
				writer.write(actual);
				writer.close();
				sc.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
